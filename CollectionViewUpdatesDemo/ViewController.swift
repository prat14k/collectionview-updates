//
//  ViewController.swift
//  CollectionViewUpdatesDemo
//
//  Created by Prateek Sharma on 6/24/19.
//  Copyright © 2019 Prateek Sharma. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    private enum Section {
        case main
    }
    @IBOutlet weak private var collectionView: UICollectionView!
    private let ds = {
        return (1...100).map { _ in  String.randomSpacedString }
    }()
    private lazy var filters = ds
    private var collectionDS: UICollectionViewDiffableDataSource<Section, String>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureDataSource()
        filter(searchText: "")
    }
    
    private func configureDataSource() {
        collectionDS = UICollectionViewDiffableDataSource(collectionView: collectionView, cellProvider: { (collectionView, indexPath, string) -> UICollectionViewCell? in
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
            for case let lbl as UILabel in cell.contentView.subviews {
                lbl.text = string
            }
            return cell
        })
    }
    
    private func filter(searchText: String) {
        if searchText.trimmingCharacters(in: .whitespaces).isEmpty {
            filters = ds
        } else {
            filters = ds.filter { $0.contains(searchText) }
        }
        let snapShot = NSDiffableDataSourceSnapshot<Section, String>()
        snapShot.appendSections([.main])
        snapShot.appendItems(filters)
        collectionDS.apply(snapShot, animatingDifferences: true)
    }
}

extension ViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.width / 2) - 20, height: 40)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    }
    
}

extension ViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filter(searchText: searchText)
    }
}
